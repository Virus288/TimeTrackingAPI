# Time tracking API

# General information

# 1. Description

#### This is simple tasks tracking API. It uses mongoDB as database. It was created for teams to coordinate their work on projects.

# 2. How to start

### 2.1 Manual way

#### After getting code, modify .env-example file by adding url to your mongoDB database.

# 3. Available routes

## 3.1 Receiving data

### Get `/get`

#### Get route was made to get data from database. It required param "type", which should be type of data and 2 optional arguments. if user want to fetch all data, send request without anythig else than type, if user want to fetch data with specified param, send request with "category" and "id" ( check 4.0 ). Example of get request

```
{
    "type": "task"
    "category": "name"
    "id": "Jakub"
}
```

### POST `/add`

#### Add route add data to database. It require "type" and "data" object ( check 4.0 and 5.0 ). Example of "add" request

```
{
    "type": "type",
    "data": {
       "requiredValueName": "value"
    }
}
```

### DELETE `/remove`

#### Remove route removes data from database. It require "type" and "id". Id has to be an id of item that user want to remove. Example of "remove" request

```
{
    "type": "type",
    "id": "id"
}
```

### PATCH `/update`

#### Update route updates data in database. It require "type" and "id". Inside of "data", use can place everything that need to be updated. Example of "update" request

```
{
    "type": "type",
    "id": "elementId",
    "data": {
        "category": "newValue"
    }
}
```

# 4. Available data

### 4.1 Available types:

#### - task

#### - user

#### - team

#### - finishedTask

### 4.2 Additional data:

#### - category = type of data that you want to fetch.

#### - id = value of type of data you want to fetch. In "find", it can be used as any value, for example as name value for users. In other cases, it has to be id of element you want to update/remove

### 4.3 Additional data for "add" route

#### data: {} = Each "add" request should include "data" object with data inside of it. If you are unsure, what's required to add, simply send empty "add" request. Server will respons with information, whats missing from your request. You can also check whats required in point 5.

# 5. Data schemas = params for add/update/find

## user

#### - name: `Requied`. String with length from 5 to 50 characters

#### - current_task: `Not requied`. This parametr is task id, which will be used as "currently working on task"

#### - position: `Not requied`. String which job is to tell user's potision in team

#### - team: `Requied`. This parametr is id of team, which user belongs to

#### - finishedTasks: `Not requied`. This is array of string, which is history of user's finished tasks

## team

#### - users: `Requied`. This is array of user id's.

## task

#### - name: `Requied`. String with length from 5 to 50 characters

#### - users: `Requied`. This is array of users id's which will work on this task

#### - teams: `Requied`. This is array of teams id's which will work on this task

#### - start_time: `Not requied`. This is Date element, which will automaticly start, at soon as task is started. It can be provided inside of data object if starting time of task should be in different time. By default. Its value is "Date.now()"

#### - finish_time: `Not requied`. This is Date element, which will create itself, at soon as task is finished ( moved to "finishedTask"). It will always be "null" inside of "task" element

## finishedTask

#### - name: `Requied`. String with length from 5 to 50 characters

#### - users: `Requied`. This is array of users id's which will work on this task

#### - teams: `Requied`. This is array of teams id's which will work on this task

#### - start_time: `Requied`. This is Date element, it is required to properly count time between start and finish

#### - finish_time: `Not requied`. This is Date element, which will create itself, at soon as task is finished. By default. Its value is "Date.now()"


## 6. What can be added:

### Different approach to getting data. Instead of fetching everything from body, user should be allowed to fetch data from url params. I am working on this way using MVC method. Or its already out. I might forget to edit this project 

### Possiblity to remove "tasksHistory" document and create 1 document with "finished" variable as boolean

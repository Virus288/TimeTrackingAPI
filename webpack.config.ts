const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/server.ts'],
    },
    target: 'node',
    externals: [nodeExternals()],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'app.bundle.js',
        libraryTarget: 'commonjs',
    },
    resolve: {
        extensions: ['.ts', '.js', '.json'],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
                exclude: ['/node_modules/'],
            },
        ],
    },
};

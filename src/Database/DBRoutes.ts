import { Router as router } from 'express';
import { get, add, remove, update } from './DBController';

export const dbRouter = router();

dbRouter.get('/get', get);

dbRouter.post('/add', add);

dbRouter.delete('/remove', remove);

dbRouter.patch('/update', update);

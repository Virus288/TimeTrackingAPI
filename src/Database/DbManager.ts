// Internal modules
import {logger} from '../logger';

// Schemas
import { Task, TaskHistory, User, Team } from './Schemas/rootSchema';

// Interfaces
import { userData, teamData, tasksData, tasksHistoryData } from './Interfaces';

export default class Database {
    private readonly id: string | undefined;
    private readonly type: string;
    private readonly data: object;
    private readonly category: string | undefined;
    private errors: { [key: string]: string; } = {};

    constructor(
        type: string,
        category: string | undefined,
        data: object,
        id: string | undefined
    ) {
        // Set up properties
        this.id = id;
        this.type = type;
        this.data = data;
        this.category = category;
    }

    // Find item
    find = async () => {
        switch (this.type) {
        case 'user':
            userData.push('_id');
            return this.findHelper(
                userData,
                User.find(this.findResponse),
                User.find({[this.category]: { $in: this.id}}, this.findResponse));
        case 'team':
            teamData.push('_id');
            return this.findHelper(
                teamData,
                Team.find(this.findResponse),
                Team.find({[this.category]: { $in: this.id}}, this.findResponse));
        case 'task':
            tasksData.push('_id');
            return this.findHelper(
                tasksData,
                Task.find(this.findResponse),
                Task.find({[this.category]: { $in: this.id}}, this.findResponse));
        case 'finishedTask':
            tasksHistoryData.push('_id');
            return this.findHelper(
                tasksHistoryData,
                TaskHistory.find(this.findResponse),
                TaskHistory.find({[this.category]: { $in: this.id}}, this.findResponse));
        default:
            return {error: true, data: 'Unknown type of data'};
        }
    }

    // Add item
    add = async () => {
        let NewItem;

        switch (this.type) {
        case 'user':
            this.checkIfDataExist(this.data, userData);

            if (Object.keys(this.errors).length > 0) {
                return {error: true, err: this.errors};
            }

            NewItem = new User(this.data);
            break;
        case 'team':
            this.checkIfDataExist(this.data, teamData);

            if (Object.keys(this.errors).length > 0) {
                return {error: true, err: this.errors};
            }

            NewItem = new Team(this.data);
            break;
        case 'task':
            this.checkIfDataExist(this.data, tasksData);

            if (Object.keys(this.errors).length > 0) {
                return {error: true, err: this.errors};
            }

            NewItem = new Task(this.data);
            break;
        case 'finishedTask':
            this.checkIfDataExist(this.data, tasksHistoryData);

            if (Object.keys(this.errors).length > 0) {
                return {error: true, err: this.errors};
            }

            NewItem = new TaskHistory(this.data);
            break;
        default:
            return {error: true, data: 'Unknown type of data'};
        }

        try {
            await NewItem.save();
            return NewItem;
        } catch (err) {
            return this.getError(err, 'mulipleElements');
        }
    }

    // Update item
    update = async () => {
        switch (this.type) {
        case 'user':
            try {
                return await User.findOneAndUpdate({_id: this.id}, {...this.data}, {new: true});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'team':
            try {
                return await Team.findOneAndUpdate({_id: this.id}, {...this.data});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'finishedTask':
            try {
                return await TaskHistory.findOneAndUpdate({_id: this.id}, {...this.data});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'task':
            try {
                return await Task.findOneAndUpdate({_id: this.id}, {...this.data});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        default:
            return {error: true, data: 'Unknown type of data'};
        }
    }

    // Remove item
    remove = async () => {
        switch (this.type) {
        case 'user':
            try {
                return await User.findOneAndRemove({_id: this.id});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'team':
            try {
                return await Team.findOneAndRemove({_id: this.id});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'finishedTask':
            try {
                return await TaskHistory.findOneAndRemove({_id: this.id});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        case 'task':
            try {
                return await Task.findOneAndRemove({_id: this.id});
            } catch (err) {
                return this.getError(err, 'oneElement');
            }
            break;
        default:
            return {error: true, data: 'Unknown type of data'};
        }
    }

    // Check if everything required exists inside of data objet before adding it to db
    private checkIfDataExist = (data: Object, requiredData: Array<string>) => {
        requiredData.forEach((item) => {
            if (!Object.keys(data).includes(item)) {
                this.addError(item, `Property '${item}' is missing`);
            }
        });
    }

    // Find data handler
    private findHelper = async (category: Array<string>, voidNoData: () => void, voidData: () => void) => {
        if (this.id === undefined) {
            try {
                return voidNoData;
            } catch (err) {
                return this.getError(err, 'mulipleElements');
            }
        } else {
            if (this.category === undefined) {
                return {error: true, data: 'Category not provided'};
            } else {
                if (!category.includes(this.category)) {
                    return {error: true, data: 'Unknown category'};
                }
                try {
                    return voidData;
                } catch (err) {
                    return this.getError(err, 'mulipleElements');
                }
            }
        }
    }

    // 'Find' data response
    private findResponse = () => {
        (err: object, data: object) => {
            if (err) {
                logger.error({message: 'Database error', dbErr: err});
                return {error: true, data: err};
            } else {
                return data;
            }
        };
    }

    // Get error and log it
    private getError = (err: any, from: string) => {
        switch (from) {
        case 'mulipleElements':
            Object.keys(err.errors).forEach((error) => {
                this.addError(error, err.message);
            });
            break;
        case 'oneElement':
            if (err.name === 'CastError') {
                this.addError(err.name, err.kind + ' has wrong format. Make sure that you are providing proper data format');
            }
            break;
        default:
            break;
        }
        // Double the error at least for now to be sure that something wont get damaged. Just need additional testing
        logger.error({message: 'Database error', dbErr: this.errors});
        logger.error({message: 'Database error', dbErr: err});
        return {error: true, data: this.errors};
    }

    // Add error to class variables
    private addError(key: string, val: string) {
        this.errors[key] = val;
    }
}

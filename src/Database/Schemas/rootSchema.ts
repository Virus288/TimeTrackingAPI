import mongoose from 'mongoose';

// Internal schemas
import { unfinishedTaskSchema } from './tasksSchema';
import { finishedTaskSchema } from './tasksHistory';
import { teamSchema } from './teamSchema';
import { userSchema } from './userSchema';

// TasksHistory
export const Task = mongoose.model('task', unfinishedTaskSchema);

// Tasks
export const TaskHistory = mongoose.model('taskHistory', finishedTaskSchema);

// Teams
export const Team = mongoose.model('team', teamSchema);

// Users
export const User = mongoose.model('user', userSchema);

import mongoose from 'mongoose';

// Team
export const teamSchema = new mongoose.Schema({
    users: {
        type: Array,
        required: true,
    },
});

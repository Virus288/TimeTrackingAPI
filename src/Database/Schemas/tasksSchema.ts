import mongoose from 'mongoose';

// Task
export const unfinishedTaskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Please enter task name'],
            maxLength: 50,
        },
        users: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Users',
            required: [true, 'Please enter user id'],
        },
        teams: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Teams',
            required: [true, 'Please enter team id'],
        },
        start_time: {
            type: Date,
            required: false,
            default: Date.now,
        },
        finish_time: {
            type: Date,
            required: false,
            default: null,
        },
    },
    { timestamps: false }
);

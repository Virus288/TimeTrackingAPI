import mongoose from 'mongoose';

// User
export const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Please enter user name'],
            maxLength: 50,
            minLength: [5, 'Min lenght of user name is 5 characters'],
        },
        current_task: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tasks',
            required: false,
            default: null,
        },
        position: {
            type: String,
            required: false,
        },
        team: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Teams',
            required: [true, 'Please enter team id'],
        },
        finishedTasks: {
            type: Array,
            required: false,
        },
    },
    { timestamps: false }
);

import express from 'express';

import Database from './DbManager';

// Get files
export const get = (req: express.Request, res: express.Response) => {
    if (req.body.type === undefined || req.body.type.length === 0) {
        return res.status(422).send({ message: "'Type' is missing" });
    }
    const Data = new Database(
        req.body.type,
        req.body.category,
        req.body.data,
        req.body.id
    );

    Data.find().then((data: any) => {
        if (data.error === true) {
            res.status(422).send(data);
        } else {
            res.status(200).send(data);
        }
    });
};

// Add files
export const add = (req: express.Request, res: express.Response) => {
    if (req.body.data === undefined) {
        return res
            .status(422)
            .send({ message: "It seems like you forgot to include 'data' object" });
    }

    const Data = new Database(
        req.body.type,
        req.body.category,
        req.body.data,
        req.body.id
    );

    Data.add().then((data) => {
        sendResponse(data, res);
    });
};

// Remove files
export const remove = (req: express.Request, res: express.Response) => {
    if (
        req.body.type === undefined ||
    req.body.type.length === 0 ||
    req.body.id === undefined ||
    req.body.id.length === 0
    ) {
        return res.status(422).send({ message: "'Type' or 'id' missing" });
    }

    const Data = new Database(
        req.body.type,
        req.body.category,
        req.body.data,
        req.body.id
    );

    Data.remove().then((data) => {
        sendResponse(data, res);
    });
};

// Remove files
export const update = (req: express.Request, res: express.Response) => {
    if (
        req.body.data === undefined ||
    Object.keys(req.body.data).length === 0 ||
    req.body.id === undefined ||
    req.body.id.length === 0
    ) {
        return res.status(422).send({
            message:
        "It seems like you forgot to include 'data' object or 'id' is missing",
        });
    }

    const Data = new Database(
        req.body.type,
        req.body.category,
        req.body.data,
        req.body.id
    );

    Data.update().then((data) => {
        sendResponse(data, res);
    });
};

// Internal function which send all data except find which has another structure
const sendResponse = (
    data: { error: boolean },
    res: express.Response<any, Record<string, any>>
) => {
    if (data === null) {
        return res
            .status(422)
            .send({ message: "Something is missing. Database returned 'not found'" });
    } else {
        if (data.error === true) {
            return res.status(422).send(data);
        } else {
            return res.status(201).send({ message: 'Success' });
        }
    }
};

// User
export interface userInterface {
  name: string;
  currentTask: string;
  position: string;
  team: string;
  finishedTasks: Array<string>;
}

export const userData: Array<string> = ['name', 'team'];

// Team
export interface teamInterface {
  users: Array<string>;
}

export const teamData: Array<string> = ['users'];

// Finished tasks
export interface finishedTasks {
  name: string;
  users: Array<string>;
  teams: Array<string>;
  startTime: Date;
  finishTime: Date;
}

export const tasksHistoryData: Array<string> = [
    'name',
    'users',
    'teams',
    'start_time',
];

// Unfinished tasks
export interface unfinishedTasks {
  name: string;
  users: Array<string>;
  teams: Array<string>;
  startTime: Date;
  finishTime: Date;
}

export const tasksData: Array<string> = ['name', 'users', 'teams'];

// External modules
import express from 'express';
import http from 'http';
import mongoose from 'mongoose';

// Internal modules
import { dbRouter } from './Database/DBRoutes';

require('dotenv').config();

const app = express();

// Database
const httpServer = http.createServer(app);

// Middleware
app.use(express.json());

// Routes
app.use(dbRouter);

// Connect to db and listen
mongoose
    .connect(process.env.MongoLocal, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
    })
    .then(() => {
        httpServer.listen(5003, () => {
            console.log('App started. Listening on http 5003');
        });
    })
    .catch((err) => console.log(err));
